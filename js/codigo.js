Vue.component('item-vue',{
    props:  ['titulo'],
    template: `
        <div align="center">
            <h1> 
                {{titulo}}
            </h1>
        </div>
    `
    
})

var i= 1;
var vm = new Vue({
    el: '#app',
    data: {
        name: '',
        desc: '',
        pre: '',
        array: []
        

    },

    methods: {
    
      errores: function () {

        //Id incremental en array  
        var cantidad = document.getElementById("tablaresultado").rows.length;
        if( cantidad === 1){
            i = 1;
            this.array.push(i++);
        }else{
            this.array.push(i++);

        }
       


        var nombre = this.name.trim();
        var desc = this.desc.trim();
        var precio = this.pre.trim();
            if (nombre === '') {
                alert("Campo de nombre vacio, intente nuevamente");
            }else if(precio === ''){
                alert("Campo de precio vacio, intente nuevamente");
            }else{
        
                var tableRef = document.getElementById("tablaresultado");
                var newRow = tableRef.insertRow(-1);

            
                var newCell= newRow.insertCell(-1);

                var newText = document.createTextNode(nombre);
                var newText2 = document.createTextNode(desc);
                var newText3 = document.createTextNode("$"+precio);

                newCell.appendChild(newText);
                var newCell2= newRow.insertCell(-1);
                newCell2.appendChild(newText2);

                var newCell3 = newRow.insertCell(-1);
                newCell3.appendChild(newText3);

                var btn = document.createElement("input");
                var btn2 = document.createElement("input");

                btn.style.width = '28%';
                btn2.style.width = '28%';

                btn.style.height= '7%';
                btn.style.height= '7%';

                btn.src = "img/boton.png";
                btn2.src= "img/boton2.png";


                btn.id = this.array[this.array.length-1];

    


                btn2.id = this.array[this.array.length-1];

        

                btn.style.paddingTop = '4px';
                btn.style.paddingLeft= '6px';
                btn2.style.padding ='5px 16px';
                btn2.style.paddingLeft= '4px 4px';

                btn.type = "image";
                btn2.type = "image";

                btn.onclick = function () {
                    actualizar(btn.id)
                    
                };

                btn2.onclick = function () {
                    eliminar(btn2.id)
                }

                var newCell4 = newRow.insertCell(-1);
                newCell4.appendChild(btn);
                newCell4.appendChild(btn2);

                //limpiar inputs
                document.getElementById("name").value = "";
                document.getElementById("desc").value = "";
                document.getElementById("pre").value = "";
            
        }
    }

}

    

  })


  function actualizar(id) {

    //actualizar el precio del producto
    var opcion;
    opcion = prompt("Introduzca el nuevo precio","precio");
    var precionuevo = document.createTextNode("$"+opcion);
    

    if(opcion === null || opcion ==='' || isNaN(opcion)){
        alert("Error, intente nuevamente");
    }else{
        
        var firstRow = document.getElementById("tablaresultado").rows[id];
        firstRow.deleteCell(2);


        var nuevacelda = firstRow.insertCell(2);
        

        nuevacelda.appendChild(precionuevo);
        
        
    }    
    

  };


  function eliminar(id){
        
    var x = document.getElementById("tablaresultado").rows.length;
    var total = x-1;
    var op;
    if (total === 1){

        op = confirm("Esta seguro que desea eliminar");
        if (op == true){
            var firstRow2 = document.getElementById("tablaresultado").deleteRow(total);
        }

        
    }else if (id == total){
       
        var nn = id-1
        op = confirm("Esta seguro que desea eliminar");
        if (op==true){
            var firstRow2 = document.getElementById("tablaresultado").deleteRow(nn);
        }
        
    }else if (id > total ){
        
        var valor = id - total;
        if (valor === 1 && total < 1 ){
            op = confirm("Esta seguro que desea eliminar");
            if (op==true){
                var firstRow2 = document.getElementById("tablaresultado").deleteRow(total-valor);
            }
            
        }else{
            op = confirm("Esta seguro que desea eliminar");
            if (op==true){
                var firstRow2 = document.getElementById("tablaresultado").deleteRow(total);
            }
        }
    }else{
        op = confirm("Esta seguro que desea eliminar");
        if (op==true){
            var firstRow2 = document.getElementById("tablaresultado").deleteRow(id);
        }
    }
  };

